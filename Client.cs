﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TCP服务端和客户端
{

  public  class MyStruct
    {
        //string cmd = $"{{\"cmd\" :\"getsn\"}}";
        public string getsn = $"{{\"cmd\" :\"getsn\"}}";
        public string ivsresult = $"{{\"cmd\" :\"ivsresult\", \"enable\" :true, \"format\" :\"json\",\"image\" :true,\"image_type\" :0}}";
        //public string getsn = $"{{\"cmd   \"    :\"    getsn  \" }}";
    }
   
public partial class Client : Form
{
    private Reader.ReaderMethod readerCamera;//摄像头采集

       
    public Client()
    {
        InitializeComponent();
        //初始化访问读写器实例
        readerCamera = new Reader.ReaderMethod();
        //回调函数
        readerCamera.ReceiveCallback = ReceiveDataCamera;
    }
        private byte[] getByteCmd(string cmd,string mode="")
        {
            List<byte> byteList = new List<byte>();
            int len = cmd.Length;

            byte a = (byte)'V';
            byte b = (byte)'Z';
            byte[] head = { a, b, 0, 0, 0, 0, 0, 0 };
            head[4] += (byte)((len >> 24) & 0xFF);
            head[5] += (byte)((len >> 16) & 0xFF);
            head[6] += (byte)((len >> 8) & 0xFF);
            head[7] += (byte)(len & 0xFF);
            byte[] b_cmd = System.Text.Encoding.ASCII.GetBytes(cmd);
            byteList.AddRange(head);
            byteList.AddRange(b_cmd);
           
            return byteList.ToArray();
        }

    private void btnopenclient_Click(object sender, EventArgs e)
    {
        string strLog = string.Empty;
        string strException = string.Empty;
        IPAddress ipAddress = IPAddress.Parse("192.168.11.42");//监听的ip地址
        int nPort = 8899;//监听的端口号
        int nRet = readerCamera.ConnectServer(ipAddress, nPort, out strException);
        if (nRet != 0)//如果连接摄像头失败
        {
            strLog = "连接摄像头失败，请确认是否已打开服务端，失败原因： " + strException;
        }
        else
        {
            strLog = "成功连接摄像头"+ipAddress+":"+nPort;
        }
        WriteLog(richTextBox1, strLog);
    }
    /// <summary>
    /// 客户端接收数据
    /// </summary>
    /// <param name="btAryReceiveData"></param>
    private void ReceiveDataCamera(byte[] btAryReceiveData)
    {
        string str = System.Text.Encoding.Default.GetString(btAryReceiveData);//数据接收转string
        //richTextBox1.AppendText("接收到数据:" + str);直接调用该方法会出现跨线程调用问题
        WriteLog(richTextBox1, "接收到数据:" + str);
    }
    #region 利用委托解决跨线程调用问题方法
    private delegate void WriteLogUnSafe(RichTextBox logRichTxt, string strLog);
    public static void WriteLog(RichTextBox logRichTxt, string strLog)
    {
        if (logRichTxt.InvokeRequired)
        {
            WriteLogUnSafe InvokeWriteLog = new WriteLogUnSafe(WriteLog);
            logRichTxt.Invoke(InvokeWriteLog, new object[] { logRichTxt, strLog });
        }
        else
        {
            logRichTxt.AppendText(strLog + "\r\n");
        }
    }
        #endregion
        private void send() { }
    private void btnclientsend_Click(object sender, EventArgs e)
    {
        readerCamera.ServerSendMessage(System.Text.Encoding.Default.GetBytes(tbclient.Text));//发送数据
    }

        private void button1_Click(object sender, EventArgs e)
        {
            //byte[] bl = getByteCmd(textBox1.Text);
            byte[] bl = getByteCmd(new MyStruct().ivsresult);
            readerCamera.ServerSendMessage(bl);
                   //readerCamera.
            textBox2.Text = System.Text.Encoding.Default.GetString(bl);
                
        }
    }
}
